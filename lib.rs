
// We don't handle io_error condition, because there's really nothing
// we can do if we get an unexpected EOF. Just bubble it up.

#[crate_id = "bitbucket.org/tari/rust-vorbis#vorbis:0.1"];
#[feature(struct_variant)];

#[feature(phase)];

#[phase(syntax, link)] extern crate log;

pub mod bitstream;
pub mod codebook;
pub mod ogg;

mod huffman;
mod util;

