extern crate vorbis;
use vorbis::bitstream::PacketDecoder;
use vorbis::ogg;

fn main() {
    let args = std::os::args();
    if args.len() < 2 {
        fail!("Requires file");
    }

    let f = match std::io::File::open(&std::path::Path::new(args[1])) {
        Ok(f) => f,
        Err(_) => fail!("Failed to open input file")
    };
    let mut oggDec = ogg::Decoder::new(f);
    
    let mut pDec = PacketDecoder::new(&mut oggDec);
    for packet in pDec {
        println!("{:?}", packet);
    }
}
