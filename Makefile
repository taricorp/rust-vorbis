RUSTC ?= rustc
RUSTFLAGS ?= -g

LIB_MAIN := lib.rs

.PHONY: all check clean clean-deps
all: libvorbis oggdec
check: check-libvorbis
clean: clean-libvorbis clean-oggdec
clean-deps:
	rm -f *.d

LIB_NAME := $(shell $(RUSTC) --crate-file-name --crate-type dylib $(LIB_MAIN))
LIB_DEP := $(patsubst %.rs,%.d,$(LIB_MAIN))
LIB_FLAGS := $(RUSTFLAGS) --crate-type dylib

$(LIB_DEP) $(LIB_NAME): $(LIB_MAIN)
	$(RUSTC) --dep-info $(LIB_DEP) $(LIB_FLAGS) $(LIB_MAIN)
-include $(LIB_DEP)

.PHONY: libvorbis check-libvorbis clean-libvorbis
libvorbis: $(LIB_NAME)

clean-libvorbis: clean-libvorbis-check
	rm -f $(LIB_NAME)
.PHONY: clean-libvorbis-check
clean-libvorbis-check:
	rm -f $(LIB_NAME).check

# Unit tests in module source
check-libvorbis: $(LIB_NAME).check
	./$(LIB_NAME).check
$(LIB_NAME).check $(LIB_TESTDEP): $(LIB_MAIN)
	$(RUSTC) $(LIB_FLAGS) --dep-info lib-check.d -o $(LIB_NAME).check --test $(LIB_MAIN)

.PHONY: clean-oggdec
oggdec: main.rs $(LIB_NAME)
	$(RUSTC) $(RUSTFLAGS) -L. main.rs -o oggdec
clean-oggdec:
	rm -f oggdec
