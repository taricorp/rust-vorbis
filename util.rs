use std::num::Float;

pub fn ilog(mut x: uint) -> uint {
    let mut log = 0;
    while x > 0 {
        log += 1;
        x >>= 1;
    }
    return log;
}

static VORBIS_EXPONENT: u32 = 10;
static VORBIS_EXPBIAS: int = 788;   // TODO 768? (spec and libvorbis disagree)
static VORBIS_MANTISSA: u32 = 21;

fn mask(n: u32) -> u32 {
    (1 << n) - 1
}

// Vorbis float32 has a wider exponent (10 bits) and shorter mantissa (21 bits)
// than an IEEE 754 single-precision float, but the loss of precision in
// this conversion is probably not an issue; libvorbis uses IEEE floats
// internally, so this conversion probably doesn't result in a meaningful
// loss of precision.
pub fn float32_unpack(x: u32) -> f32 {
    let sign = x & 0x80000000 != 0;
    let mut mantissa = (x & mask(VORBIS_MANTISSA)) as f32;
    let mut exponent = (x & (mask(VORBIS_EXPONENT) << VORBIS_MANTISSA)) as int;
    exponent >>= VORBIS_MANTISSA;

    if sign {
        mantissa = -mantissa;
    }
    Float::ldexp(mantissa, exponent as int - VORBIS_EXPBIAS)
}
