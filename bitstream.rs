#[phase(syntax, link)] extern crate log;

use std::{cmp, str};
use std::vec::Vec;
use std::io::{IoResult, IoError, InvalidInput};

use codebook::Codebook;

pub struct BitReader<'a, T> {
    priv input: &'a mut T,
    // Must be wide enough to allow read() to return 32 bits at a time.
    priv cache: u64,
    priv cache_avail: u8,
}

impl<'a, T: Reader> BitReader<'a, T> {
    fn fill_buffer(&mut self) -> IoResult<()> {
        let b = try!(self.input.read_u8());
        self.cache |= (b as u64) << self.cache_avail;
        self.cache_avail += 8;
        Ok(())
    }

    pub fn align(&mut self) -> uint {
        let padding = self.cache_avail % 8;
        self.cache >>= padding;
        self.cache_avail -= padding;

        padding as uint
    }

    pub fn read_bits(&mut self, n: u8) -> IoResult<uint> {
        // Fill buffer
        while self.cache_avail < n {
            try!(self.fill_buffer());
        }
        // Get bottom n bits of cache
        let mask = (1u64 << n) - 1;
        let out = self.cache & mask;
        self.cache_avail -= n;
        self.cache >>= n;
        return Ok(out as uint);
    }

    pub fn new(input: &'a mut T) -> BitReader<'a, T> {
        BitReader {
            input: input,
            cache: 0,
            cache_avail: 0
        }
    }
}

impl<'a, T: Reader> Reader for BitReader<'a, T> {
    fn read(&mut self, buf: &mut [u8]) -> IoResult<uint> {
        if self.cache_avail % 8 != 0 {
            // Can't force alignment. This is going to be slow.
            // TODO when len(buf) is large, we can do larger reads and buffer
            // more. Limitation is that we must not read more than requested,
            // to avoid blocking/sudden EOF.
            let mut i = 0;
            while i < buf.len() {
                let x = try!(self.read_bits(8));
                buf[i] = x as u8;
                i += 1;
            }
            return Ok(i);
        }

        // Flush the cache
        let flushCount = cmp::min(buf.len(), self.cache_avail as uint / 8);
        let mut n = 0;
        while n < flushCount {
            buf[n] = self.cache as u8;
            n += 1;
            self.cache >>= 8;
        }
        // Read what's left
        let out = self.input.read(buf.mut_slice_from(n));
        out.map(|count| count + n)
    }
}

pub struct PacketDecoder<'a, T> {
    stream: BitReader<'a, T>,
}

impl<'a, T: Reader> PacketDecoder<'a, T> {
    pub fn new(r: &'a mut T) -> PacketDecoder<'a, T> {
        PacketDecoder {
            stream: BitReader::new(r),
        }
    }

    fn read_packet(&mut self) -> IoResult<Packet> {
        let isHeader = try!(self.stream.read_bits(1)) == 1;

        if isHeader {
            // Header packet
            let htype = try!(self.stream.read_bits(7)) << 1 | 1;
            debug!("Begin Vorbis header packet, type {}", htype);

            // Signature
            let sig = try!(self.stream.read_exact(6));
            if bytes!("vorbis") != sig {
                return Err(IoError {
                    kind: InvalidInput,
                    desc: "Packet signature is not \"vorbis\"",
                    detail: None
                });
            }

            let out = match htype {
                1 => Identification {
                        vorbisVersion: try!(self.stream.read_be_u32()),
                        audioChannels: try!(self.stream.read_u8()),
                        audioSampleRate: try!(self.stream.read_le_u32()),
                        bitrateMaximum: try!(self.stream.read_le_i32()),
                        bitrateNominal: try!(self.stream.read_le_i32()),
                        bitrateMinimum: try!(self.stream.read_le_i32()),
                        blocksize0: try!(self.stream.read_bits(4)) as u8,
                        blocksize1: try!(self.stream.read_bits(4)) as u8,
                        framingFlag: try!(self.stream.read_bits(1)) == 1
                    },

                3 => { 
                    let vendorLength = try!(self.stream.read_le_u32());
                    let vendorBytes = try!(self.stream.read_exact(vendorLength as uint));
                    let vendor = str::from_utf8_lossy(vendorBytes).into_owned();

                    let commentCount = try!(self.stream.read_le_u32()) as uint;
                    let mut comments: Vec<~str> = Vec::with_capacity(commentCount);
                    for _ in range(0, commentCount) {
                        let len = try!(self.stream.read_le_u32()) as uint;
                        let comment_raw = try!(self.stream.read_exact(len));
                        let comment = str::from_utf8_lossy(comment_raw).into_owned();
                        comments.push(comment);
                    }
                    
                    Comment {
                        vendor: vendor,
                        comments: comments,
                        framingFlag: try!(self.stream.read_bits(1)) == 1
                    }
                }

                5 => {
                    let codebookCount = try!(self.stream.read_u8()) as uint + 1;
                    debug!("Reading {} codebooks", codebookCount);
                    let mut codebookConfigurations = Vec::with_capacity(codebookCount);
                    for _ in range(0, codebookCount) {
                        codebookConfigurations.push(Codebook::read_from(&mut self.stream));
                    }
                    
                    fail!("htype 5 not yet complete");
                }

                // TODO
                v @ _ => fail!("Bad header packet type: {}", v)
            };
            // Packets are always byte-aligned
            let dropped = self.stream.align();
            debug!("Realigning bitstream; dropped {} bits", dropped);
            return Ok(out);
        } else {
            //AudioPacket::decodeFrom(self.stream)
            Err(::std::io::IoError {
                kind: ::std::io::OtherIoError,
                desc: "Audio packet decode not implemented",
                detail: None
            })
        }
    }
}

impl<'a, T: Reader> Iterator<Packet> for PacketDecoder<'a, T> {
    fn next(&mut self) -> Option<Packet> {
        match self.read_packet() {
            Err(e) => {
                error!("IO error while reading packet: {}", e);
                None
            }
            Ok(p) => Some(p)
        }
    }
}


pub enum Packet {
    Identification {
        vorbisVersion: u32,
        audioChannels: u8,
        audioSampleRate: u32,
        bitrateMaximum: i32,
        bitrateNominal: i32,
        bitrateMinimum: i32,

        blocksize0: u8,
        blocksize1: u8,
        framingFlag: bool
    },

    Comment {
        vendor: ~str,
        comments: Vec<~str>,
        framingFlag: bool
    },

    Setup,
    Audio
}

/*struct VorbisDecoder {

}*/

/*impl Drop for VorbisDecoder {
    fn drop(&self) {

    }
}*/
