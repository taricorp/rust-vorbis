#[phase(syntax, link)] extern crate log;

use std::{fmt, cmp};
use std::io::{IoResult, IoError, EndOfFile};
use std::iter::AdditiveIterator;

pub struct Page {
    // Unpacked from header_type_flags
    continuedPacket: bool,
    bOS: bool,                      // Beginning-of-stream
    eOS: bool,                      // End-of-stream

    absoluteGranulePosition: u64,
    streamSerialNumber: u32,
    pageSequenceNumber: u32,

    size: uint,
    checksum: u32
}

impl fmt::Show for Page {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut flags = ~"";
        if self.continuedPacket {
            flags.push_str(" CONT");
        }
        if self.bOS {
            flags.push_str(" BGN");
        }
        if self.eOS {
            flags.push_str(" END");
        }

        write!(f.buf, "Page(position={} streamID={:x} seqNum={} size={}{})",
               self.absoluteGranulePosition, self.streamSerialNumber,
               self.pageSequenceNumber, self.size, flags)
    }
}

// TODO a way to callback on next() makes a lot of sense.
// It could enable:
//  * stream switching- callback fn indicates which client gets the page
//  * debug output or something?
pub struct Decoder<T> {
    priv input: T,
    priv eOS: bool,

    priv lastPage: Option<Page>,
    priv lastPageTaken: uint
}

impl<T: Reader> Decoder<T> {
    pub fn new(r: T) -> Decoder<T> {
        Decoder {
            input: r,
            eOS: false,

            lastPage: None,
            lastPageTaken: 0,
        }
    }

    fn io_next(&mut self) -> IoResult<Page> {
        // Skip trailing payload on last page, if any
        match self.lastPage {
            Some(p) => {
                try!(self.input.read_exact(p.size - self.lastPageTaken));
            }
            None => ()
        }

        // Sync to page signature
        // TODO try to resync
        let capture = try!(self.input.read_exact(4));
        if bytes!("OggS") != capture {
            fail!("Invalid page capture pattern");
        }

        // stream_structure_version: 0
        if try!(self.input.read_u8()) != 0 {
            fail!("Invalid stream structure version");
        }

        let typeFlags = try!(self.input.read_u8());
        if typeFlags & 4 != 0 {
            self.eOS = true;
        }

        let absPos = try!(self.input.read_le_u64());
        let seqNum = try!(self.input.read_le_u32());
        let pageNum = try!(self.input.read_le_u32());
        let checksum = try!(self.input.read_le_u32());

        let segments = try!(self.input.read_u8()) as uint;
        let lacing = try!(self.input.read_exact(segments));
        let pageSize = lacing.iter().map(|&x| x as uint).sum();

        let p = Page {
            continuedPacket: typeFlags & 1 != 0,
            bOS: typeFlags & 2 != 0,
            eOS: typeFlags & 4 != 0,

            absoluteGranulePosition: absPos,
            streamSerialNumber: seqNum,
            pageSequenceNumber: pageNum,

            size: pageSize,
            checksum: checksum
        };
        self.eOS = p.eOS;

        self.lastPage = Some(p);
        self.lastPageTaken = 0;
        debug!("New OGG page {:?}", p);
        return Ok(p);
    }
}

impl<T: Reader> Reader for Decoder<T> {
    // TODO a mode to yield None on end-of-page (force next()) might be useful.
    fn read(&mut self, buf: &mut [u8]) -> IoResult<uint> {
        if self.eOS {
            return Err(IoError {
                kind: EndOfFile,
                desc: "Got end-of-stream flag in last page.",
                detail: None
            });
        }

        // Number of bytes read
        let mut n: uint = 0;
        while n < buf.len() {
            // Get a new page if we need more data
            self.lastPage = match self.lastPage {
                Some(p) => {
                    if self.lastPageTaken == p.size {
                        debug!("Reached end of OGG page {}", p.pageSequenceNumber);
                        self.next()
                    } else {
                        self.lastPage
                    }
                },
                None => self.next()
            };

            match self.lastPage {
                Some(p) => {
                    assert!(self.lastPageTaken <= p.size)

                    // Read from input bounded by page size
                    let count = cmp::min(buf.len() - n, p.size - self.lastPageTaken);
                    let dst = buf.mut_slice(n, n + count);
                    let count = try!(self.input.read(dst));

                    n += count;
                    self.lastPageTaken += count;
                },
                None => break
            }
        }
        return Ok(n);
    }
}

impl<T: Reader> Iterator<Page> for Decoder<T> {
    fn next(&mut self) -> Option<Page> {
        match self.io_next() {
            Ok(page) => Some(page),
            Err(e) => {
                error!("I/O error in ogg page read: {}", e);
                None
            }
        }
    }
}
