use std::iter;
use std::mem;
use std::vec::Vec;
use log;

type ValidateResult = Result<(), ~str>;

pub trait HuffmanTree<T> {
    /// Look up a left-aligned codeword.
    ///
    /// Returns the number of bits consumed from the codeword and the
    /// corresponding value.
    ///
    /// Codeword value should be left-aligned in the parameter, so the string
    /// "010" becomes 0x40000000, and "11101100" is 0xEC000000.
    fn lookup(&self, codeword: u32) -> (u8, T);

    /// Constructs a Huffman tree with specified codewords.
    /// 
    /// `lengths` specifies the set of codeword lengths, which are assigned
    /// leftmost-first such that each permutation of codeword lengths describes
    /// a unique tree.
    fn assemble(codes: &[i8]) -> Self;

    fn validate(&self) -> ValidateResult;
}

// TODO exceedingly inefficient representation.
/// A linked binary tree.
pub enum BinaryTree<T> {
    Leaf(T),
    Branch(Option<~BinaryTree<T>>,Option<~BinaryTree<T>>)
}

impl BinaryTree<u32> {
    fn install(t: &mut Option<~BinaryTree<u32>>,
                     code: u32, len: u8, value: u32) {
        if len == 0 {
            assert!(t.is_none());
            mem::replace(t, Some(~Leaf(value)));
            return;
        }
        // Install a new branch if nothing here yet.
        if t.is_none() {
            mem::replace(t, Some(~Branch(None, None)));
        }

        let next = match *t {
            Some(~Leaf(..)) => fail!("Attempted to traverse leaf"),
            Some(~Branch(ref mut left, ref mut right)) => {
                if code & (1 << (len - 1)) == 0 {
                    left
                } else {
                    right
                }
            },
            None => unreachable!()
        };
        BinaryTree::install(next, code, len - 1, value);
    }

    #[cfg(debug)]
    /// Emits a text representation of the tree which can be rendered
    /// graphically with a tool such as Graphviz's `dot`.
    fn dump_dot<W: Writer>(&self, w: &mut W) {
        fn recurse<W: Writer>(tree: &BinaryTree<u32>,
                              w: &mut W,
                              serial: &mut uint) {
            match *tree {
                Leaf(v) => {
                    let _ = w.write_str(format!("n{} [label=\"{}\"];", *serial, v));
                    *serial += 1;
                },
                Branch(ref left, ref right) => {
                    let s = *serial;
                    let _ = w.write_str(format!("n{} [label=\"\"];", s));

                    for (i, &branch) in vec!(left, right).iter().enumerate() {
                        match *branch {
                            Some(ref t) => {
                                *serial += 1;
                                let _ = w.write_str(format!("n{} -> n{} [label=\"{}\"];",
                                                            s, *serial, i));
                                recurse(*t, w, serial);
                            },
                            None => ()
                        }
                    }
                }
            };
        };

        let mut serial: uint = 0;
        recurse(self, w, &mut serial);
    }
}

#[deriving(Clone, Eq, Show)]
struct Codeword<T> {
    code: u32,
    len: u8,
    value: T
}

fn assign(lengths: &[i8]) -> Vec<Codeword<u32>> {
    if log_enabled!(log::DEBUG) {
        debug!("Assembling tree with {} codewords", lengths.len());
        debug!("{}", lengths);
    }

    // TODO This condition is probably not sufficient
    if lengths.len() == 1 && lengths[0] == 0 {
        // Pseudo-nil tree is valid but silly.
        return vec!(Codeword {
            code: 0,
            len: 0,
            value: 0
        });
    }

    // Right-aligned next codewords. The high n - 1 bits of claims[n]
    // are always clear.
    let mut claims = [0u32, ..33];
    let mut symbols: Vec<Codeword<u32>> = Vec::with_capacity(lengths.len());

    for (i, &len) in lengths.iter().enumerate() {
        assert!(len > 0);
        assert!(len <= 32);
        // Put this aside; we use it for pruning children
        let mut code = claims[len];
        symbols.push(Codeword {
            code: code,
            len: len as u8,
            value: i as u32
        });

        // Sanity check: an overflowing addition in the parent update will
        // violate the length constraint on `claims`, indicating an overfull
        // tree.
        // Length 32 overfills are uncatchable.
        // TODO maybe they are if we do a checked add.
        if len < 32 && (code >> len) != 0 {
            fail!("Tree is overfull. Tried to add node at len = {} (i = {})", len, i);
        }

        // Update parents: cascade increment through all parents that pointed
        // to a child updated by this process.
        for i in iter::range_step(len, 0, -1) {
            if claims[i] & 1 != 0 {
                // This completes a subtree. Jump to the rightmost sibling
                // of our parent.
                if i == 1 {
                    // (but the root's parent has no siblings)
                    claims[1] += 1;
                } else {
                    claims[i] = claims[i - 1] << 1;
                }
                // Next parent must already point to the jump we just made.
                break;
            } else {
                claims[i] += 1;
            }
        }

        // Prune children which have the node we just claimed as an ancestor,
        // cascading to max depth.
        for i in iter::range(len + 1, 33) {
            if (claims[i] >> 1) == code {
                code = claims[i];
                claims[i] = claims[i - 1] << 1;
            } else {
                break;
            }
        }
    }

    // TODO sanity check of total count against levels. Sum should be exactly
    // UINT32_MAX.
    
    symbols
}

impl HuffmanTree<u32> for BinaryTree<u32> {
    fn assemble(lengths: &[i8]) -> BinaryTree<u32> {
        let codes = assign(lengths);
        let mut tree: Option<~BinaryTree<u32>> = None;

        for &codeword in codes.iter() {
            match codeword {
                Codeword { len: len, code: code, value: value } => {
                    BinaryTree::install(&mut tree, code, len, value);
                 }
            }
        }

        match tree {
            Some(~t) => {
                //if cfg!(paranoid) {
                    match t.validate() {
                        Ok(..) => (),
                        Err(e) => fail!("Constructed Huffman tree is not valid: {}", e)
                    }
                //}
                t
            },
            None => unreachable!()
        }
    }

    fn lookup(&self, codeword: u32) -> (u8, u32) {
        match *self {
            Leaf(v) => (0, v),
            Branch(ref left, ref right) => {
                let target = if (codeword & (1 << 31)) == 0 {
                    left
                } else {
                    right
                };
                match *target {
                    Some(ref t) => {
                        let (d, v) = t.lookup(codeword << 1);
                        (d + 1, v)
                    },
                    None => fail!("Lookup reached non-leaf terminal; tree is incomplete")
                }
            }
        }
    }

    fn validate(&self) -> ValidateResult {
        match *self {
            Leaf(..) => Ok(()),
            Branch(Some(~ref left), Some(~ref right)) => {
                try!(left.validate());
                try!(right.validate());
                Ok(())
            }
            Branch(None, None) => Err(~"Branch has no children"),
            Branch(None, _) => Err(~"Branch is missing left child"),
            Branch(_, None) => Err(~"Branch is missing right child")
        }
    }
}

#[test]
fn test_assign_basic() {
    let codes = assign([1,1]);
    assert_eq!(*codes.get(0), Codeword {
        code: 0,
        len: 1,
        value: 0
    });
    assert_eq!(*codes.get(1), Codeword {
        code: 1,
        len: 1,
        value: 1
    });
}

#[test]
fn test_assign_partials() {
    let codes = assign([2,1,2]);
    assert_eq!(*codes.get(0), Codeword {
        code: 0,
        len: 2,
        value: 0
    });
    assert_eq!(*codes.get(1), Codeword {
        code: 1,
        len: 1,
        value: 1
    });
    assert_eq!(*codes.get(2), Codeword {
        code: 1,
        len: 2,
        value: 2
    });
}

#[test]
fn test_assemble_simple() {
    // This tree is the one in Figure 4 of the Vorbis I spec.
    let lengths = [2,4,4,4,4,2,3,3];
    let tree: BinaryTree<u32> = HuffmanTree::assemble(lengths);

    assert_eq!(tree.lookup(0), (2, 0));
    assert_eq!(tree.lookup(4 << 28), (4, 1));
    assert_eq!(tree.lookup(5 << 28), (4, 2));
    assert_eq!(tree.lookup(6 << 28), (4, 3));
    assert_eq!(tree.lookup(7 << 28), (4, 4));
    assert_eq!(tree.lookup(8 << 28), (2, 5));
    assert_eq!(tree.lookup(12 << 28), (3, 6));
    assert_eq!(tree.lookup(14 << 28), (3, 7));
}

#[test]
fn test_assemble_long() {
    let mut lengths = [0i8, ..33];
    lengths[0] = 32;
    for (i, x) in iter::range_step::<i8>(32, 0, -1).enumerate() {
        lengths[i + 1] = x;
    }

    let tree: BinaryTree<u32> = HuffmanTree::assemble(lengths);
    assert_eq!(tree.lookup(0), (32, 0));
    assert_eq!(tree.lookup(1), (32, 1));
    assert_eq!(tree.lookup(2), (31, 2));
    assert_eq!(tree.lookup(1 << 30), (2, 31));
    assert_eq!(tree.lookup(1 << 31), (1, 32));
}

#[test]
fn test_assemble_1078() {
    let tree: BinaryTree<u32> = HuffmanTree::assemble([2, 1, 2]);
    assert_eq!(tree.lookup(0), (2, 0));
    assert_eq!(tree.lookup(1 << 31), (1, 1));
    assert_eq!(tree.lookup(1 << 30), (2, 2));
}

#[test]
fn test_assemble_zero() {
    let tree: BinaryTree<u32> = HuffmanTree::assemble([0]);
    assert_eq!(tree.lookup(0), (0, 0));
    assert_eq!(tree.lookup(-1 as u32), (0, 0));
}

#[test]
#[should_fail]
fn test_assemble_missing() {
    let _: BinaryTree<u32> = HuffmanTree::assemble([]);
}

#[test]
#[should_fail]
fn test_assemble_oversize() {
    let _: BinaryTree<u32> = HuffmanTree::assemble([1, 2, 3, 42]);
}
