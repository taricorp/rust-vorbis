
use std::io::IoResult;
use std::vec::Vec;

use bitstream::BitReader;
use huffman::{HuffmanTree, BinaryTree};
use util::{ilog, float32_unpack};

pub struct Codebook {
    // TODO VQ and non-VQ codebooks are not interchangeable (can't use non-VQ
    // codebooks to yield vectors). May be a better abstraction for this.
    priv isVQ: bool,
    priv tree: BinaryTree<u32>
}

impl Codebook {
    pub fn read_from<R: Reader>(stream: &mut BitReader<R>) -> IoResult<Codebook> {
        let sync = try!(stream.read_le_uint_n(3));
        assert!(sync == 0x564342);
        // Codeword lengths are 5 bits, so codewords may be up to 32 bits long
        // TODO use dimensions for.. *something*.
        // libvorbis passes a 'sparsecount' into _make_words (part of our
        // assemble() process) that it's not clear what the use is.
        let (dimensions, lengths) = try!(Codebook::read_lengths(stream));
        debug!("Codeword lengths: {:?}", lengths);
        let tree: BinaryTree<u32> = HuffmanTree::assemble(lengths.as_slice());

        let lookupType = try!(stream.read_bits(4));
        debug!("Reading vector lookup table type {}", lookupType);
        assert!(lookupType <= 2);
        if lookupType != 0 {
            // TODO unpack & endianness check
            let minimum = float32_unpack(try!(stream.read_be_u32()));
        }

        Ok(Codebook {
            isVQ: lookupType != 0,
            tree: tree
        })
    }

    fn read_lengths<T: Reader>(stream: &mut BitReader<T>) -> IoResult<(u16, Vec<i8>)> {
        let dimensions = try!(stream.read_le_u16());
        debug!("Codebook dimensions are {}", dimensions);
        let entries = try!(stream.read_le_uint_n(3)) as uint;
        debug!("Codebook has {} codewords", entries);
        let ordered = try!(stream.read_bits(1)) == 1;
        debug!("Codebook entries ordered? {}", ordered);

        // Fill lengths
        let mut codewordLengths: Vec<i8> = Vec::with_capacity(entries);
        if ordered {
            // Codeword list encoded in ascending order
            let mut i = 0;
            let mut len = try!(stream.read_bits(5)) as i8 + 1;
            debug!("Ordered CW len = {}", len);
            while i < entries {
                let n = try!(stream.read_bits(ilog(entries - i) as u8));
                for _ in range(i, i + n) {
                    codewordLengths.push(len);
                }
                i += n;
                len += 1;
            }
            assert!(i == entries);  // Extra entries provided; stream broken
        } else {
            // Unordered codeword list
            let sparse = try!(stream.read_bits(1)) == 1;

            for _ in range(0, entries) {
                let present = if sparse {
                    1 == try!(stream.read_bits(1))
                } else {
                    true
                };
                // "Unused" entries have no codeword, so it appears we can
                // simply ignore them.
                // XXX may need to preserve unused entries though.
                if present {
                    let len = try!(stream.read_bits(5)) as i8 + 1;
                    codewordLengths.push(len);
                }
            }
        }
        return Ok((dimensions, codewordLengths));
    }
}
